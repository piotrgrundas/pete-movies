from django.urls import path

from .views import CommentListView, MovieListView, MovieRankView


urlpatterns = [
    path('movies/', MovieListView.as_view(), name='movies'),
    path('comments/', CommentListView.as_view(), name='comments'),
    path('top/', MovieRankView.as_view(), name='movies-top'),
]
