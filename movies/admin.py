from django.contrib import admin

from .models import Movie, Rating, Comment


class RatingInline(admin.StackedInline):
    model = Rating
    classes = ('collapse',)
    extra = 0


class CommentInline(admin.StackedInline):
    model = Comment
    classes = ('collapse',)
    extra = 0
    readonly_fields = ('added',)


class CommentAdmin(admin.ModelAdmin):
    readonly_fields = ('added',)
    list_display = ('movie', 'added')

class MovieAdmin(admin.ModelAdmin):
    inlines = [
        RatingInline,
        CommentInline,
    ]


admin.site.register(Movie, MovieAdmin)
admin.site.register(Rating)
admin.site.register(Comment, CommentAdmin)
