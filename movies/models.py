from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=255, unique=True)
    year = models.CharField(max_length=25)
    rated = models.CharField(max_length=50)
    released = models.DateField()
    runtime = models.CharField(max_length=25)
    genre = models.CharField(max_length=255)
    director = models.CharField(max_length=255)
    writer = models.CharField(max_length=500)
    actors = models.CharField(max_length=1000)
    plot = models.TextField()
    language = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    awards = models.CharField(max_length=255)
    website = models.CharField(max_length=255, blank=True)
    metascore = models.CharField(max_length=25)
    imdb_rating = models.CharField(max_length=25)
    imdb_votes = models.CharField(max_length=25)
    imdb_id = models.CharField(max_length=25)
    type = models.CharField(max_length=50)
    dvd = models.CharField(max_length=100, blank=True)
    box_office = models.CharField(max_length=100, blank=True)
    production = models.CharField(max_length=100, blank=True)
    poster = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.title} ({self.year})'


class Rating(models.Model):
    source = models.CharField(max_length=255)
    value = models.CharField(max_length=50)
    movie = models.ForeignKey(
        Movie,
        models.CASCADE,
        null=False,
        blank=False,
        related_name='ratings'
    )

    def __str__(self):
        return f'{self.movie} rating'


class Comment(models.Model):
    comment = models.TextField()
    movie = models.ForeignKey(
        Movie,
        models.CASCADE,
        null=False,
        blank=False,
        related_name='comments',
    )
    added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.movie} comment'
