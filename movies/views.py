from datetime import datetime

from django.db.models import F, Count, Q
from django.db.models.expressions import Window
from django.db.models.functions import DenseRank
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.response import Response

from providers import movies_provider
from .serializers import (
    CreateMovieSerializer,
    MovieSerializer,
    CommentSerializer,
    MovieRankSerializer
)
from .models import Movie, Comment


class MovieListView(ListCreateAPIView):
    create_serializer_class = CreateMovieSerializer
    serializer_class = MovieSerializer
    queryset = Movie.objects.prefetch_related('ratings', 'comments')
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_fields = ('year', 'title', 'language', 'type')
    ordering_fields = ('year', 'title', 'language', 'type')

    def create(self, request, *args, **kwargs):
        create_serializer = self.create_serializer_class(data=request.data)
        create_serializer.is_valid(raise_exception=True)

        movie_data = movies_provider.movie_details(create_serializer.validated_data['title'])
        serializer = self.serializer_class(data=movie_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CommentListView(ListCreateAPIView):
    serializer_class = CommentSerializer
    queryset =  Comment.objects.order_by('movie', 'added')
    filter_fields = ('movie',)


class MovieRankView(ListAPIView):
    date_filter_format = '%d-%m-%Y'
    serializer_class = MovieRankSerializer

    def get_queryset(self):
        extra = {}
        date_from = self.request.query_params.get('date_from')
        date_to = self.request.query_params.get('date_to')

        try:
            if date_from:
                extra['comments__added__gte'] = datetime.strptime(
                    date_from,
                    self.date_filter_format
                )
            if date_to:
                extra['comments__added__lte'] = datetime.strptime(
                    date_to,
                    self.date_filter_format
                )
        except ValueError:
            pass

        return Movie.objects.all(
        ).annotate(
            total_comments=Count(
                'comments',
                **({'filter': Q(**extra)} if extra else extra)
            )
        ).annotate(
            rank=Window(
                expression=DenseRank(),
                order_by=F('total_comments').desc()
            )
        ).order_by('rank')
