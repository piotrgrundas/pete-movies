import operator

from django.test import TestCase
from django.urls import reverse
from mock import ANY, patch
from rest_framework.test import APIClient


from movies.models import Comment, Movie, Rating
from providers.exceptions import MovieNotFoundException


class ViewsBaseTestCase(TestCase):
    fixtures = [
        'movies/fixtures/Movie.json',
        'movies/fixtures/Comment.json',
        'movies/fixtures/Rating.json',
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.api = APIClient()


class TestMovieListView(ViewsBaseTestCase):
    def test_get(self):
        response = self.api.get(reverse('movies'), data={'ordering': 'title'})
        self.assertEqual(response.status_code, 200)
        json = response.json()
        self.assertEqual(len(json), 4)
        self.assertEqual(
            json[0],
            {
                "id": 4,
                "released": "19 Sep 1976",
                "comments": [],
                "ratings": [
                    {
                        "source": "Internet Movie Database",
                        "value": "6.0/10"
                    }
                ],
                "title": "Cos",
                "year": "1976–",
                "rated": "N/A",
                "runtime": "60 min",
                "genre": "Comedy",
                "director": "N/A",
                "writer": "N/A",
                "actors": "Kareem Abdul-Jabbar, Muhammad Ali, Bea Arthur, Lynda Carter",
                "plot": "Sketch comedy series hosted by Bill Cosby.",
                "language": "English",
                "country": "USA",
                "awards": "N/A",
                "website": "",
                "metascore": "N/A",
                "imdb_rating": "6.0",
                "imdb_votes": "17",
                "imdb_id": "tt0073976",
                "type": "series",
                "dvd": "",
                "box_office": "",
                "production": "",
                "poster": "N/A"
            },
        )

    @patch('movies.views.movies_provider.movie_details')
    def test_post(self, mock_movie_details):
        mock_movie_details.return_value = {
            'title': 'New',
            'released': '03 Sep 2010',
            'year': '2000',
            'rated': 'Y',
            'runtime': '5000',
            'genre': 'drama',
            'director': 'N/A',
            'writer': 'Me',
            'actors': 'None',
            'plot': 'super fantastic',
            'language': 'FR',
            'country': 'FRONSE',
            'awards': 'Wat?',
            'metascore': 'hmm',
            'website': 'N/A',
            'imdb_rating': 'N/A',
            'imdb_votes': 'N/A',
            'imdb_id': 'N/A',
            'type': 'N/A',
            'dvd': 'N/A',
            'box_office': 'N/A',
            'production': 'N/A',
            'poster': 'N/A',
            'ratings': [
                {'source': 'source1', 'value': 'value2'},
                {'source': 'source1', 'value': 'value2'},
            ]
        }

        self.assertEqual(4, Movie.objects.count())

        response = self.api.post(reverse('movies'), {'title': 'New'})
        self.assertEqual(response.status_code, 201)
        json = response.json()
        self.assertEqual(
            response.json(),
            {
                'comments': [],
                'id': ANY,
                **mock_movie_details.return_value
            }
        )
        self.assertEqual(5, Movie.objects.count())
        self.assertEqual(2, Rating.objects.filter(movie_id=json['id']).count())

        response = self.api.post(reverse('movies'), {'title': 'New'})
        self.assertEqual(
            response.json(),
            {
                'title': ['movie with this title already exists.']
            }
        )

        mock_movie_details.side_effect = MovieNotFoundException('Movie not found!')
        response = self.api.post(reverse('movies'), {'title': 'xxx'})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(
            response.json(),
            {'detail': 'Movie not found!'}
        )

    def test_filters(self):
        self.assertEqual(
            len(self.api.get(reverse('movies')).json()),
            4
        )

        response = self.api.get(reverse('movies'), data={'language': 'English'})
        self.assertEqual(response.status_code, 200)
        json = response.json()
        self.assertEqual(len(json), 2)
        self.assertEqual(
            tuple(map(operator.itemgetter('language'), json)),
            ('English', 'English')
        )

        response = self.api.get(reverse('movies'), data={'type': 'series'})
        json = response.json()
        self.assertEqual(len(json), 1)
        self.assertEqual(json[0]['type'], 'series')

        response = self.api.get(reverse('movies'), data={'year': '2010'})
        json = response.json()
        self.assertEqual(len(json), 1)
        self.assertEqual(json[0]['year'], '2010')

        response = self.api.get(reverse('movies'), data={'title': 'Tekken'})
        json = response.json()
        self.assertEqual(len(json), 1)
        self.assertEqual(json[0]['title'], 'Tekken')


class TestCommentListView(ViewsBaseTestCase):
    def test_post(self):
        movie_id = 2
        comment = { 'comment': 'Comment', 'movie': movie_id }

        response = self.api.get(reverse('comments'), data={'movie': movie_id})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 2)
        self.assertEqual(2, Comment.objects.filter(movie_id=movie_id).count())

        response = self.api.post(reverse('comments'), comment)
        self.assertEqual(
            response.json(),
            {
                'comment': 'Comment',
                'added': ANY,
                'movie': 2
            }
        )
        self.assertEqual(3, Comment.objects.filter(movie_id=movie_id).count())

    def test_get(self):
        response = self.api.get(reverse('comments'))
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            response.json(),
            [
                {
                    'comment': 'bla bla 2111',
                    'added': '2018-10-17T14:38:39.300000Z',
                    'movie': 2
                },
                {
                    'comment': 'bla bla 2111',
                    'added': '2018-10-18T14:38:39.300000Z',
                    'movie': 2
                },
                {
                    'comment': 'bla bla 12312312',
                    'added': '2018-10-15T14:38:39.300000Z',
                    'movie': 3
                },
                {
                    'comment': 'bla bla 12312312',
                    'added': '2018-10-19T14:38:39.300000Z',
                    'movie': 3
                }
            ]
        )

    def test_filters(self):
        response = self.api.get(
            reverse('comments'),
            content_type='application/json',
            data={'movie': 3}
        )
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            response.json(),
            [
                {
                    'comment': 'bla bla 12312312',
                    'added': '2018-10-15T14:38:39.300000Z',
                    'movie': 3
                },
                {
                    'comment': 'bla bla 12312312',
                    'added': '2018-10-19T14:38:39.300000Z',
                    'movie': 3
                }
            ]
        )

        response = self.api.get(
            reverse('comments'),
            content_type='application/json',
            data={'movie': 9999}
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {
                'movie': [
                    'Select a valid choice. That choice is not one of the available choices.'
                ]
            }
        )

class TestMovieRankView(ViewsBaseTestCase):
    def test_get(self):
        response = self.api.get(reverse('movies-top'))
        self.assertEqual(response.status_code, 200)
        self.assertCountEqual(
            response.json(),
            [
                {
                    'movie_id': 2,
                    'total_comments': 2,
                    'rank': 1
                },
                {
                    'movie_id': 3,
                    'total_comments': 2,
                    'rank': 1
                },
                {
                    'movie_id': 1,
                    'total_comments': 0,
                    'rank': 2
                },
                {
                    'movie_id': 4,
                    'total_comments': 0,
                    'rank': 2
                }]
        )

    def test_filters(self):
        response = self.api.get(
            reverse('movies-top'),
            data={'date_from': '18-10-2018'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertCountEqual(
            response.json(),
            [
                {
                    'movie_id': 2,
                    'total_comments': 1,
                    'rank': 1
                },
                {
                    'movie_id': 3,
                    'total_comments': 1,
                    'rank': 1
                },
                {
                    'movie_id': 1,
                    'total_comments': 0,
                    'rank': 2
                },
                {
                    'movie_id': 4,
                    'total_comments': 0,
                    'rank': 2
                }
            ]
        )

        response = self.api.get(
            reverse('movies-top'),
            data={
                'date_from': '18-10-2018',
                'date_to': '19-10-2018'
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertCountEqual(
            response.json(),
            [
                {
                    'movie_id': 2,
                    'total_comments': 1,
                    'rank': 1
                },
                {
                    'movie_id': 1,
                    'total_comments': 0,
                    'rank': 2
                },
                {
                    'movie_id': 3,
                    'total_comments': 0,
                    'rank': 2
                },
                {
                    'movie_id': 4,
                    'total_comments': 0,
                    'rank': 2
                }
            ]

        )

        response = self.api.get(reverse('movies-top'), data={'date_from': 'wrong'})
        self.assertEqual(response.status_code, 200)

        response = self.api.get(reverse('movies-top'), data={'date_to': 'format'})
        self.assertEqual(response.status_code, 200)
