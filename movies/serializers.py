from rest_framework import serializers

from .models import Comment, Movie, Rating

DATE_FORMAT = '%d %b %Y'


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        exclude = ('id', 'movie')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        exclude = ('id',)


class CreateMovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('title',)


class MovieSerializer(serializers.ModelSerializer):
    released = serializers.DateField(
        input_formats=[DATE_FORMAT],
        format=DATE_FORMAT
    )
    comments = CommentSerializer(many=True, required=False)
    ratings = RatingSerializer(many=True, required=False)

    class Meta:
        model = Movie
        fields = '__all__'

    def create(self, validated_data):
        ratings = validated_data.pop('ratings', [])
        movie = Movie.objects.create(**validated_data)
        for rating in ratings:
            Rating.objects.create(movie=movie, **rating)
        return movie


class MovieRankSerializer(serializers.Serializer):
    movie_id = serializers.SerializerMethodField()
    total_comments = serializers.SerializerMethodField()
    rank = serializers.SerializerMethodField()

    def get_movie_id(self, obj):
        return obj.id

    def get_total_comments(self, obj):
        return obj.total_comments

    def get_rank(self, obj):
        return obj.rank
