# Simple movie API

###Development:
Setup postgres, and create db:

    $ psql postgres
    $ CREATE ROLE pete WITH LOGIN PASSWORD 'pete'
    $ CREATE DATABASE pete-movies
    $ GRANT ALL PRIVILEGES ON DATABASE petemovies TO pete

Clone the repo
    
    $ git clone git@bitbucket.org:piotrgrundas/pete-movies.git

Create env
   
    $ mkvirtualenv pete-movies -p $(which python3.7) && cd pete-movies && setvirtualenvproject 

Install requirements

    $ pip install -r requirements.txt

Apply migrations

    $ ./manage.py migrate

Run dev server

    $ ./manage.py runserver --settings=settings.dev


Tests
    
    $ ./manage.py test --settings=settings.dev -v 2 -k
