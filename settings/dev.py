from .prod import *

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = INSTALLED_APPS + [
    'django_extensions'
]

MIDDLEWARE = MIDDLEWARE + [
    'querycount.middleware.QueryCountMiddleware'
]

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s: %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'providers.base': {
            'handlers': ['console'],
            'level': 'DEBUG'
        },
    }
}

QUERYCOUNT = {
    'THRESHOLDS': {
        'MEDIUM': 50,
        'HIGH': 200,
        'MIN_TIME_TO_LOG':0,
        'MIN_QUERY_COUNT_TO_LOG':0
    },
    'IGNORE_REQUEST_PATTERNS': [],
    'IGNORE_SQL_PATTERNS': [],
    'DISPLAY_DUPLICATES': None,
    'RESPONSE_HEADER': 'X-DjangoQueryCount-Count'
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'petemovies',
        'USER': 'pete',
        'PASSWORD': 'pete',
        'HOST': 'localhost',
        'PORT': '',
    }
}
