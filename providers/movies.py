import requests

from .base import BaseProvider
from .exceptions import MovieNotFoundException
from utils.parsers import camel_case_to_underscore_recursive


class MoviesProvider(BaseProvider):

    def _call(self, endpoint, method=requests.get, **kwargs):
        params = kwargs.pop('params', {})
        params['apikey'] = self.API_KEY
        response = super()._call(endpoint, method=requests.get, params=params, **kwargs)
        json = response.json()

        if 'Error' in json:
            raise MovieNotFoundException(json['Error'])
        return camel_case_to_underscore_recursive(json)

    def movie_details(self, title):
        return self._call(params={'t': title}, endpoint='/')
