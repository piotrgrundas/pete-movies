from rest_framework.exceptions import NotFound


class MovieNotFoundException(NotFound):
    pass
