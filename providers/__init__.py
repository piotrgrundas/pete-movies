from django.conf import settings

from .movies import MoviesProvider


movies_provider = MoviesProvider(
    base_url=settings.MOVIES_SERVICE_URL,
    api_key=settings.MOVIES_SERVICE_API_KEY,
    verify=settings.MOVIES_SERVICE_VERIFY,
)
