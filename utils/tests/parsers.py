import unittest

from ..parsers import camel_case_to_underscore_recursive


class ParsersTestCase(unittest.TestCase):
    def test_recursive_key_map(self):
        test_data = {
            'TestTest': 1,
            'TestCase': 2,
            'InnerTest': {
                'TestAgain': {
                    'EvenDeeperTest': '5'
                }
            }
        }
        self.assertDictEqual(
            camel_case_to_underscore_recursive(test_data),
            {
                'test_test': 1,
                'test_case': 2,
                'inner_test': {
                    'test_again': {
                        'even_deeper_test': '5'
                    }
                }
            }
        )

        test_data2 = [
            {'TestCase': 1},
            {'TestCase': 2},
        ]
        self.assertEqual(
            camel_case_to_underscore_recursive(test_data2),
            [
                {'test_case': 1},
                {'test_case': 2},
            ]
        )
